<?php
/*
* Copyright 2022 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>

<?php

namespace common	ests\unit\models;

use Codeception\Test\Unit;
use common\models\Station;
use common\models\StationFs;
use common\models\StationWigig;
use Mockery\Mock;


class StationCalcScenario0Test extends Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var $stationW Station|Mock
     */
    protected $stationWA;

    /**
     * @var $stationW Station|Mock
     */
    protected $stationWB;

    /**
     * @var $stationW StationWigig|Mock
     */
    protected $wigigA;
    /**
     * @var $stationW StationWigig|Mock
     */
    protected $wigigB;

    protected $cDone = false;

    protected $wigigData = [
        8 => [
            'id' => 12,
            'lat' => 48.0420279056,
            'lng' => 18.55914855,
            'antenna_volume' => 43,
            'power' => 10,
            'channel_width' => 2000,
            'type' => 'wigig',
            'wigig' =>
                [
                    'eirp' => 40,
                    'direction' => 23,
                    'is_ptmp' => true
                ],
            'out' =>
                [
                    'inConflict' => false,
                    'diff' => 0
                ]
        ],
        1 => [
            'id' => 1,
            'lat' => 48.0424483418,
            'lng' => 18.5589661598,
            'antenna_volume' => 43,
            'power' => 10,
            'channel_width' => 2000,
            'type' => 'wigig',
            'wigig' =>
                [
                    'eirp' => 55,
                    'direction' => 110,
                    'is_ptmp' => false
                ],
            'out' =>
                [
                    'inConflict' => false,
                    'diff' => 0
                ]
        ],
        11 => [
            'id' => 11,
            'lat' => 48.0420728326,
            'lng' => 18.5595616102,
            'antenna_volume' => 43,
            'power' => 10,
            'channel_width' => 2000,
            'type' => 'wigig',
            'wigig' =>
                [
                    'eirp' => 40,
                    'direction' => 0,
                    'is_ptmp' => true
                ],
            'out' =>
                [
                    'inConflict' => false,
                    'diff' => 0
                ]
        ],
    ];

    protected function _before()
    {
        $this->prepareStationPair();
    }

    protected function prepareStationPair()
    {
        /**
         * @var $stationWA Station|Mock
         */
        $this->stationWA = \Mockery::mock(Station::class)->makePartial();
        $this->stationWA->shouldReceive('hasAttribute')->andReturn(true);
        $this->wigigA = \Mockery::mock(StationWigig::class)->makePartial();
        $this->wigigA->shouldReceive('hasAttribute')->andReturn(true);

        $this->stationWA->shouldReceive('getStationTypeObject')->andReturn($this->wigigA);

        /**
         * @var $stationWB Station|Mock
         */
        $this->stationWB = \Mockery::mock(Station::class)->makePartial();
        $this->stationWB->shouldReceive('hasAttribute')->andReturn(true);
        $this->wigigB = \Mockery::mock(StationWigig::class)->makePartial();
        $this->wigigB->shouldReceive('hasAttribute')->andReturn(true);

        $this->stationWB->shouldReceive('getStationTypeObject')->andReturn($this->wigigB);

    }

    public function testScenario0Station81()
    {
        $item = $this->wigigData[8];
        $this->makeWigigFromConfig($this->stationWA, $this->wigigA,  $this->wigigData[1]);
        $this->makeWigigFromConfig($this->stationWB, $this->wigigB,  $this->wigigData[8]);

        $comp = $this->stationWA->compareWith($this->stationWB);
        $this->assertEquals($item['out']['inConflict'], $comp['passiveInterference']);
        $this->assertEquals($item['out']['diff'], round($comp['difference'], 4));
    }


    protected function makeWigigFromConfig(&$station, &$wigig, $config)
    {
        foreach ($config as $key => $val) {
            if ($key != 'wigig' || $key != 'out') {
                $station->$key = $val;
            }
        }

        foreach ($config['wigig'] as $key => $val) {
            $wigig->$key = $val;
        }
    }
}
