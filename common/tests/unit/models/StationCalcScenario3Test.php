<?php
/*
* Copyright 2022 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>

<?php

namespace common	ests\unit\models;

use Codeception\Test\Unit;
use common\models\Station;
use common\models\StationFs;
use common\models\StationWigig;
use Mockery\Mock;


class StationCalcScenario3Test extends Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var $fsVA StationFs|Mock
     */
    protected $fsVA;

    /**
     * @var $fsVB StationFs|Mock
     */
    protected $fsVB;

    /**
     * @var $fsAA StationFs|Mock
     */
    protected $fsAA;

    /**
     * @var $fsAB StationFs|Mock
     */
    protected $fsAB;


    /**
     * @var $aggressorA Station|Mock
     */
    protected $aggressorA;

    /**
     * @var $aggressorB Station|Mock
     */
    protected $aggressorB;

    /**
     * @var $victimA Station|Mock
     */
    protected $victimA;

    /**
     * @var $victimB Station|Mock
     */
    protected $victimB;


    protected $cDone = false;

    protected $fsData = [
        2241 => [
            'a' =>
                [
                    'id' => 22,
                    'lat' => 50.0425683707,
                    'lng' => 14.5601302385,
                    'antenna_volume' => 30,
                    'power' => 5,
                    'channel_width' => 50,
                    'type' => 'fs',
                    'fs' => [
                        'frequency' => 61000,
                        'ratio_signal_interference' => 25,
                        'specific_run_down' => 13,

                    ],
                ],
            'b' =>
                [
                    'id' => 41,
                    'lat' => 50.042162016,
                    'lng' => 14.5615839958,
                    'antenna_volume' => 30,
                    'power' => 5,
                    'channel_width' => 50,
                    'type' => 'fs',
                    'fs' => [
                        'frequency' => 61000,
                        'ratio_signal_interference' => 25,
                        'specific_run_down' => 13,

                    ],
                ]
        ],
        2332 => [
            'a' =>
                [
                    'id' => 23,
                    'lat' => 50.0425965339,
                    'lng' => 14.5603233576,
                    'antenna_volume' => 30,
                    'power' => 5,
                    'channel_width' => 50,
                    'type' => 'fs',
                    'fs' => [
                        'frequency' => 61000,
                        'ratio_signal_interference' => 25,
                        'specific_run_down' => 13,

                    ],
                ],
            'b' =>
                [
                    'id' => 32,
                    'lat' => 50.0417107344,
                    'lng' => 14.5606505871,
                    'antenna_volume' => 30,
                    'power' => 5,
                    'channel_width' => 50,
                    'type' => 'fs',
                    'fs' => [
                        'frequency' => 61000,
                        'ratio_signal_interference' => 25,
                        'specific_run_down' => 13,

                    ],
                ]
        ],
    ];

    protected $configurations = [
//        'ap' => ['aggressor' => ['id' => 2241, 'a' => 'a', 'b' => 'b'], 'victim' => ['id' => 2332, 'a' => 'a', 'b' => 'b'], 'inConflict' => false, 'difference' => -23.419],
        'ap' => ['aggressor' => ['id' => 2241, 'a' => 'a', 'b' => 'b'], 'victim' => ['id' => 2332, 'a' => 'a', 'b' => 'b'], 'inConflict' => false, 'difference' => -18.1421],
//        'pa' => ['aggressor' => ['id' => 2332, 'a' => 'a', 'b' => 'b'], 'victim' => ['id' => 2241, 'a' => 'a', 'b' => 'b'], 'inConflict' => false, 'difference' => -24.588],
        'pa' => ['aggressor' => ['id' => 2332, 'a' => 'a', 'b' => 'b'], 'victim' => ['id' => 2241, 'a' => 'a', 'b' => 'b'], 'inConflict' => false, 'difference' => -19.2706],

//        'aq' => ['aggressor' => ['id' => 2241, 'a' => 'a', 'b' => 'b'], 'victim' => ['id' => 2332, 'a' => 'b', 'b' => 'a'], 'inConflict' => false, 'difference' => -23.717],
        'aq' => ['aggressor' => ['id' => 2241, 'a' => 'a', 'b' => 'b'], 'victim' => ['id' => 2332, 'a' => 'b', 'b' => 'a'], 'inConflict' => false, 'difference' => -24.5815],
//        'qa' => ['aggressor' => ['id' => 2332, 'a' => 'b', 'b' => 'a'], 'victim' => ['id' => 2241, 'a' => 'a', 'b' => 'b'], 'inConflict' => false, 'difference' => -24.886],
        'qa' => ['aggressor' => ['id' => 2332, 'a' => 'b', 'b' => 'a'], 'victim' => ['id' => 2241, 'a' => 'a', 'b' => 'b'], 'inConflict' => false, 'difference' => -25.71],


        'bp' => ['aggressor' => ['id' => 2241, 'a' => 'b', 'b' => 'a'], 'victim' => ['id' => 2332, 'a' => 'a', 'b' => 'b'], 'inConflict' => false, 'difference' => 0],
        'pb' => ['aggressor' => ['id' => 2332, 'a' => 'a', 'b' => 'b'], 'victim' => ['id' => 2241, 'a' => 'b', 'b' => 'a'], 'inConflict' => false, 'difference' => 0],

        'bq' => ['aggressor' => ['id' => 2241, 'a' => 'b', 'b' => 'a'], 'victim' => ['id' => 2332, 'a' => 'b', 'b' => 'a'], 'inConflict' => false, 'difference' => 0],
        'qb' => ['aggressor' => ['id' => 2332, 'a' => 'b', 'b' => 'a'], 'victim' => ['id' => 2241, 'a' => 'b', 'b' => 'a'], 'inConflict' => false, 'difference' => 0],
    ];

    protected function _before()
    {
    }

    protected function prepareStationPair($aggressor, $victim)
    {
        $this->aggressorA = \Mockery::mock(Station::class)->makePartial();
        $this->fsAA = \Mockery::mock(StationFs::class)->makePartial();
        $this->fsAA->shouldReceive('hasAttribute')->andReturn(true);
        $this->aggressorA->shouldReceive('getStationTypeObject')->andReturn($this->fsAA);
        $this->aggressorA->shouldReceive('hasAttribute')->andReturn(true);

        $this->aggressorB = \Mockery::mock(Station::class)->makePartial();
        $this->fsAB = \Mockery::mock(StationFs::class)->makePartial();
        $this->fsAB->shouldReceive('hasAttribute')->andReturn(true);
        $this->aggressorB->shouldReceive('getStationTypeObject')->andReturn($this->fsAB);
        $this->aggressorB->shouldReceive('hasAttribute')->andReturn(true);

        $this->aggressorA->stationPair = $this->aggressorB;
        $this->aggressorB->stationPair = $this->aggressorA;

        $this->victimA = \Mockery::mock(Station::class)->makePartial();
        $this->fsVA = \Mockery::mock(StationFs::class)->makePartial();
        $this->fsVA->shouldReceive('hasAttribute')->andReturn(true);
        $this->victimA->shouldReceive('getStationTypeObject')->andReturn($this->fsVA);
        $this->victimA->shouldReceive('hasAttribute')->andReturn(true);

        $this->victimB = \Mockery::mock(Station::class)->makePartial();
        $this->fsVB = \Mockery::mock(StationFs::class)->makePartial();
        $this->fsVB->shouldReceive('hasAttribute')->andReturn(true);
        $this->victimB->shouldReceive('getStationTypeObject')->andReturn($this->fsVB);
        $this->victimB->shouldReceive('hasAttribute')->andReturn(true);

        $this->initFs($aggressor, $victim);


        $this->victimA->stationPair = $this->victimB;
        $this->victimB->stationPair = $this->victimA;
    }

    protected function initFs($aggressor, $victim)
    {
        $agDataA = $this->fsData[$aggressor['id']][$aggressor['a']];
        $agDataB = $this->fsData[$aggressor['id']][$aggressor['b']];
        $this->makeFsFromConfig($this->aggressorA, $this->fsAA, $agDataA);
        $this->makeFsFromConfig($this->aggressorB, $this->fsAB, $agDataB);

        $vicDataA = $this->fsData[$victim['id']][$victim['a']];
        $vicDataB = $this->fsData[$victim['id']][$victim['b']];
        $this->makeFsFromConfig($this->victimA, $this->fsVA, $vicDataA);
        $this->makeFsFromConfig($this->victimB, $this->fsVB, $vicDataB);
    }

    public function testScenario3AP()
    {
        $conf = $this->configurations['ap'];
        $this->prepareStationPair($conf['aggressor'], $conf['victim']);

        $comp = $this->victimA->compareWith($this->aggressorA);
        $this->assertEquals($conf['inConflict'], $comp['passiveInterference']);
        $this->assertEquals($conf['difference'], round($comp['difference'], 4));
    }

    public function testScenario3PA()
    {
        $conf = $this->configurations['pa'];
        $this->prepareStationPair($conf['aggressor'], $conf['victim']);

        $comp = $this->victimA->compareWith($this->aggressorA);

        $this->assertEquals($conf['inConflict'], $comp['passiveInterference']);
        $this->assertEquals($conf['difference'], round($comp['difference'], 4));
    }

    public function testScenario3AQ()
    {
        $conf = $this->configurations['aq'];
        $this->prepareStationPair($conf['aggressor'], $conf['victim']);

        $comp = $this->victimA->compareWith($this->aggressorA);
        $this->assertEquals($conf['inConflict'], $comp['passiveInterference']);
        $this->assertEquals($conf['difference'], round($comp['difference'], 4));
    }

    public function testScenario3QA()
    {
        $conf = $this->configurations['qa'];
        $this->prepareStationPair($conf['aggressor'], $conf['victim']);

        $comp = $this->victimA->compareWith($this->aggressorA);
        $this->assertEquals($conf['inConflict'], $comp['passiveInterference']);
        $this->assertEquals($conf['difference'], round($comp['difference'], 4));
    }


    protected function makeFsFromConfig(&$stationFs, &$fs, $config)
    {
        foreach ($config as $key => $val) {
            if ($key != 'fs') {
                $stationFs->$key = $val;
            }
        }

        foreach ($config['fs'] as $key => $val) {
            $fs->$key = $val;
        }
    }
}
