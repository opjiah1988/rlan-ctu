<?php
/*
* Copyright 2022 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>

<?php

namespace common	ests\unit\models;

use Codeception\Test\Unit;
use common\models\Station;
use common\models\StationFs;
use common\models\StationWigig;
use Mockery\Mock;


class StationCalcScenario2Test extends Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var $stationW Station|Mock
     */
    protected $stationW;

    /**
     * @var $stationW StationWigig|Mock
     */
    protected $wigig;

    /**
     * @var $fsA StationFs|Mock
     */
    protected $fsA;

    /**
     * @var $fsB StationFs|Mock
     */
    protected $fsB;

    /**
     * @var $stationB Station|Mock
     */
    protected $stationB;

    /**
     * @var $stationA Station|Mock
     */
    protected $stationA;

    protected $cDone = false;

    protected $fsData = [
        612 => [
            'b' =>
                [
                    'id' => 6,
                    'lat' => 50.0413694233,
                    'lng' => 14.5586174726,
                    'antenna_volume' => 45,
                    'power' => 5,
                    'channel_width' => 200,
                    'type' => 'fs',
                    'fs' => [
                        'frequency' => 61000,
                        'specific_run_down' => 13,

                        'ratio_signal_interference' => 28,
                    ],
                    'out' => [
                        'inConflict' => false,
                        'diff' => -13.203
                    ]
                ]
            ,
            'a' =>
                [
                    'id' => 12,
                    'lat' => 50.0425066799,
                    'lng' => 14.5593953133,
                    'antenna_volume' => 43,
                    'power' => 10,
                    'channel_width' => 200,
                    'type' => 'fs',
                    'fs' => [
                        'frequency' => 61000,
                        'ratio_signal_interference' => 28,
                        'specific_run_down' => 13,

                    ],
                    'out' => [
                        'inConflict' => false,
                        'diff' => 0
                    ]
                ]
        ]
    ];

    protected $wigigData = [
        8 => [
            'id' => 12,
            'lat' => 50.0420279056,
            'lng' => 14.55914855,
            'antenna_volume' => 43,
            'power' => 10,
            'channel_width' => 2000,
            'type' => 'wigig',
            'wigig' =>
                [
                    'eirp' => 40,
                    'direction' => 23,
                    'is_ptmp' => true
                ],
            'out' =>
                [
                    'inConflict' => false,
                    'diff' => -13.203
                ]
        ],
        1 => [
            'id' => 1,
            'lat' => 50.0424483418,
            'lng' => 14.5589661598,
            'antenna_volume' => 43,
            'power' => 10,
            'channel_width' => 2000,
            'type' => 'wigig',
            'wigig' =>
                [
                    'eirp' => 55,
                    'direction' => 110,
                    'is_ptmp' => false
                ],
            'out' =>
                [
                    'inConflict' => false,
                    'diff' => -30.723
                ]
        ],
        11 => [
            'id' => 11,
            'lat' => 50.0420728326, 
            'lng' => 14.5595616102,
            'antenna_volume' => 43,
            'power' => 10,
            'channel_width' => 2000,
            'type' => 'wigig',
            'wigig' =>
                [
                    'eirp' => 40,
                    'direction' => 0,
                    'is_ptmp' => true
                ],
            'out' =>
                [
                    'inConflict' => false,
                    'diff' => -32.9679
                ]
        ],
    ];

    protected function _before()
    {
        $this->prepareStationPair();
    }

    protected function prepareStationPair()
    {
        /**
         * @var $stationW Station|Mock
         */
        $this->stationW = \Mockery::mock(Station::class)->makePartial();
        $this->stationW->shouldReceive('hasAttribute')->andReturn(true);
        $this->wigig = \Mockery::mock(StationWigig::class)->makePartial();
        $this->wigig->shouldReceive('hasAttribute')->andReturn(true);

        $this->stationW->shouldReceive('getStationTypeObject')->andReturn($this->wigig);

        $this->stationA = \Mockery::mock(Station::class)->makePartial();
        $this->fsA = \Mockery::mock(StationFs::class)->makePartial();
        $this->fsA->shouldReceive('hasAttribute')->andReturn(true);
        $this->stationA->shouldReceive('getStationTypeObject')->andReturn($this->fsA);
        $this->stationA->shouldReceive('hasAttribute')->andReturn(true);

        $this->stationB = \Mockery::mock(Station::class)->makePartial();
        $this->fsB = \Mockery::mock(StationFs::class)->makePartial();
        $this->fsB->shouldReceive('hasAttribute')->andReturn(true);
        $this->stationB->shouldReceive('hasAttribute')->andReturn(true);

        $this->stationB->shouldReceive('getStationTypeObject')->andReturn($this->fsB);

        $this->initFs();

        $this->stationA->stationPair = $this->stationB;
        $this->stationB->stationPair = $this->stationA;



    }

    protected function initFs()
    {
        $item = $this->fsData[612];
        $this->makeFsFromConfig($this->stationA, $this->fsA, $item['a']);
        $this->makeFsFromConfig($this->stationB, $this->fsB, $item['b']);
    }

    public function testScenario2Station8()
    {
        $item = $this->wigigData[8];
        $this->makeWigigFromConfig($this->stationW, $this->wigig, $item);

        $comp = $this->stationW->compareWith($this->stationA);
        $this->assertEquals($item['out']['inConflict'], $comp['passiveInterference']);
        $this->assertEquals($item['out']['diff'], round($comp['difference'], 4));
    }

    public function testScenario2Station1()
    {
        $item = $this->wigigData[1];
        $this->makeWigigFromConfig($this->stationW, $this->wigig, $item);

        $comp = $this->stationW->compareWith($this->stationA);
        $this->assertEquals($item['out']['inConflict'], $comp['passiveInterference']);
        $this->assertEquals($item['out']['diff'], round($comp['difference'], 4));
    }

    public function testScenario2Station11()
    {
        $item = $this->wigigData[11];
        $this->makeWigigFromConfig($this->stationW, $this->wigig, $item);

        $comp = $this->stationW->compareWith($this->stationA);
        $this->assertEquals($item['out']['inConflict'], $comp['passiveInterference']);
        $this->assertEquals($item['out']['diff'], round($comp['difference'], 4));
    }


    protected function makeFsFromConfig(&$stationFs, &$fs, $config)
    {
        foreach ($config as $key => $val) {
            if ($key != 'fs' || $key != 'out') {
                $stationFs->$key = $val;
            }
        }

        foreach ($config['fs'] as $key => $val) {
            $fs->$key = $val;
        }
    }

    protected function makeWigigFromConfig(&$stationFs, &$wigig, $config)
    {
        foreach ($config as $key => $val) {
            if ($key != 'wigig' || $key != 'out') {
                $stationFs->$key = $val;
            }
        }

        foreach ($config['wigig'] as $key => $val) {
            $wigig->$key = $val;
        }
    }
}
