<?php
/*
* Copyright 2022 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>

<?php


namespace common\models;


use Yii;
use yii\dbxception;

trait CalculatorTrait
{


    public function getCompareType()
    {
        /** @var $this Station */

        if ($this->isFs()) {
            return self::COMPARE_TYPE_FS;
        } elseif ($this->type == Station::COMPARE_TYPE_WIGIG) {
            return self::COMPARE_TYPE_WIGIG;
        } else {
            return null;
        }
    }

    /**
     * THIS = station which is in the map already but it is close to  -> checking
     *
     * @param Station $modelToAdd - model which I want to add
     *
     * @return array
     * @throws Exception
     */
    public function compareWith(&$modelToAdd): array
    {
        /** @var $this Station */
        /*
         * Scenario |   adding   |   checking (in the system already)
         *    0     |    Wigig   |  Wigig
         *    1     |    Wigig   |  FS
         *    2     |      FS    |  Wigig
         *    3     |      FS    |  FS
         *
         * WF = Wigig with frequency
         * Wigig - normal wigig without frequency
         * note : Wigig - wigig is ok
         */
        $oldStation = $this;

        $oldType = $oldStation->getCompareType();
        $addingType = $modelToAdd->getCompareType();
        $out = [];
        if ($addingType == self::COMPARE_TYPE_WIGIG && $oldType == self::COMPARE_TYPE_WIGIG) {
            // scenario 0
            // $modelToAdd = WIGIG
            // $checking = $this = WIGIG
            // NO conflict
            $out = ['passiveInterference' => false, 'difference' => 0];

        } elseif ($addingType == self::COMPARE_TYPE_WIGIG && $oldType == self::COMPARE_TYPE_FS) {
            // scenario 1
            // $modelToAdd is wigig -> scenario 1
            // $modelToAdd = WIGIG
            // $checking = $this = FS
            $stationWigig = $modelToAdd;

            $out = $this->compareWigigFs($this, $stationWigig);

        } elseif ($addingType == self::COMPARE_TYPE_FS && $oldType == self::COMPARE_TYPE_WIGIG) {
            // scenario 2
            // $modelToAdd = FS
            // $checking = $this - Wigig

            $out = $oldStation->compareWigigFs($modelToAdd, $oldStation);

        } elseif ($addingType == self::COMPARE_TYPE_FS && $oldType == self::COMPARE_TYPE_FS) {
            // scenario 3

            // $modelToAdd = aggressor
            // $this = victim
            $out = $this->compareFsFs($modelToAdd, $this);

        }

        $this->hasConflicts |= $this->passiveInterference || $this->activeInterference;
        $modelToAdd->hasConflicts |= $this->passiveInterference || $this->activeInterference;

        return $out;
    }

    public function countFSPL($distance)
    {
        if ($this->isFs()) {
            /**
             * @var $fs StationFs
             */
            $fs = $this->getStationTypeObject();
            $lattgasPerKm = $fs->specific_run_down * $distance;

            $out = 92.4 + 20 * log($distance * ($fs->frequency / 1000), 10) + $lattgasPerKm;
            return $out;
        } else {
            throw new Exception('counting FSPL on non-FS object');
        }
    }

    public function countCNumber(): float
    {
        if ($this->isFs()) {

            $pair = $this->stationPair;
            $fs = $this->getStationTypeObject();

            /**
             * @var $fs StationFs
             */
            $distanceFsAFsB = self::countDistance($this->lat, $this->lng, $pair->lat, $pair->lng);
            // C

            $fsplC = $this->countFSPL($distanceFsAFsB);


            //      G1                     +            G2                      -  FSPL  +  P1
            $cTemp = $this->antenna_volume + $this->stationPair->antenna_volume - $fsplC + $this->power;

            $c = $cTemp - $fs->ratio_signal_interference;  // = iMax_A

            return $c;
        } else {
            throw new Exception('counting C number on non-FS object');
        }
    }


    /**
     * @param Station $aggressor
     * @param Station $victim
     *
     * @return array
     * @throws Exception
     */
    public function compareFsFs(Station &$aggressor, Station &$victim): array
    {
        /**
         * @var $fsAg StationFs
         * @var $fsVictim StationFs
         */

        $fsAg = $aggressor->getStationTypeObject();
        $fsVictim = $victim->getStationTypeObject();
        if (self::isOverlapping($fsAg->frequency, $aggressor->channel_width, $fsVictim->frequency, $victim->channel_width)) {
            $distanceFsFs = self::countDistance($victim->lat, $victim->lng, $aggressor->lat, $aggressor->lng);

            $fspl = $victim->countFSPL($distanceFsFs);
            $pairAggressor = $aggressor->stationPair;
            $pairVictim = $victim->stationPair;

            // aggressor-pair X spojnice agresor-victim
            $lambda = self::countAngleWigigFs($aggressor->lat, $aggressor->lng, $pairAggressor->lat, $pairAggressor->lng, $victim->lat, $victim->lng);

            // victim-pair X spojnice agresor-victim
            $theta = self::countAngleWigigFs($victim->lat, $victim->lng, $pairVictim->lat, $pairVictim->lng, $aggressor->lat, $aggressor->lng);

            $aggressorAdLambda = self::countAdPhi($lambda, $aggressor->antenna_volume);

            $aggressorAdTheta = self::countAdPhi($theta, $victim->antenna_volume);

            // 91 victim
            // 79 aggressor
            // I = Pfsptp79 + Gfsptp79 - AD(PHI)fsptp79 + Gfsptp91 - AD(PHI)fsptp91 – FSPL

            $rw = $aggressor->antenna_volume + $victim->antenna_volume - $aggressorAdLambda - $aggressorAdTheta - $fspl;
            $iVictim = $aggressor->power + $rw;
            $iAggressor = $victim->power + $rw;
            $cVictim = $victim->countCNumber();
            $cAggressor = $aggressor->countCNumber();


            // passiveInterference = ja rusim
            $victim->passiveInterference = ($iVictim >= $cVictim);
            $victim->passiveIsolationIncreaseRequested = $iVictim - $cVictim;

            $aggressor->passiveInterference = ($iAggressor >= $cAggressor);
            $aggressor->passiveIsolationIncreaseRequested = $iAggressor - $cAggressor;

            $victim->activeInterference = ($iAggressor >= $cAggressor);
            $victim->activeIsolationIncreaseRequested = $iAggressor - $cAggressor;

            $aggressor->activeInterference = ($iVictim >= $cVictim);
            $aggressor->activeIsolationIncreaseRequested = $iVictim - $cVictim;
        } else {
            $this->overlapping = false;
        }

        return [
            'passiveInterference' => $aggressor->passiveInterference,
            'difference' => $aggressor->passiveIsolationIncreaseRequested,
        ];
    }

    /**
     * says if there is an overlapping between two stations
     *
     * @param float|int $aggressorFrequency
     * @param float|int $aggressorChannelWidth
     * @param float|int $victimFrequency
     * @param float|int $victimChannelWidth
     *
     * @return bool
     */
    public static function isOverlapping(
        $aggressorFrequency,
        $aggressorChannelWidth,
        $victimFrequency,
        $victimChannelWidth
    ): bool
    {
        $aLeft = $aggressorFrequency - ($aggressorChannelWidth / 2);
        $aRight = $aggressorFrequency + ($aggressorChannelWidth / 2);

        $bLeft = $victimFrequency - ($victimChannelWidth / 2);
        $bRight = $victimFrequency + ($victimChannelWidth / 2);

        $out = ($bRight > $aLeft && $bRight < $aRight) ||
            ($bLeft > $aLeft && $bLeft < $aRight) ||
            ($bLeft >= $aLeft && $bRight <= $aRight) ||
            ($aLeft >= $bLeft && $aRight <= $bRight);
        return $out;
    }

    /**
     * @param Station $stationFs
     * @param Station $stationWigig
     *
     * @return array
     * @throws Exception
     */
    public function compareWigigFs(&$stationFs, &$stationWigig): array
    {
        /**
         * @var $pair Station
         */
        $pair = $stationFs->stationPair;

        $distanceWigigFs = self::countDistance($stationFs->lat, $stationFs->lng, $stationWigig->lat,
            $stationWigig->lng);

        $fsplI = $stationFs->countFSPL($distanceWigigFs);

        $c = $stationFs->countCNumber();
        // I
        /**
         * @var $wigig StationWigig
         */
        $wigig = $stationWigig->getStationTypeObject();

        $phi = self::countAngleWigigFs($stationFs->lat, $stationFs->lng, $pair->lat, $pair->lng, $stationWigig->lat,
            $stationWigig->lng);
        $omega = self::countOmega($stationFs->lat, $stationFs->lng, $stationWigig->lat, $stationWigig->lng,
            $wigig->direction);
        $adPhi = self::countAdPhi($phi, $stationFs->antenna_volume);


        $ad = self::countAdPhiWigig($omega, $wigig->is_ptmp);
        $eirpAd = $wigig->eirp - $ad;
        $deltaF = ($stationWigig->channel_width > $stationFs->channel_width) ?
            10 * log($stationWigig->channel_width / $stationFs->channel_width, 10) : 0;

        //   eirp    + FS_antenna_volume          - $adPhi - fspl   - deltaF
        $i = $eirpAd + $stationFs->antenna_volume - $adPhi - $fsplI - $deltaF;

        $difference = $i - $c;

        $stationFs->passiveInterference = ($i >= $c);
        $stationFs->passiveIsolationIncreaseRequested = $difference;

        $stationWigig->activeInterference = ($i >= $c);
        $stationWigig->activeIsolationIncreaseRequested = $difference;

        return ['passiveInterference' => ($i >= $c), 'difference' => $difference];
    }


    /**
     * counts angle between two vectors where $fsA is in the middle
     *
     * @param $ALat - point Ay
     * @param $ALng - point Ax
     * @param $BLat - point By
     * @param $BLng - point Bx
     * @param $WLat - point Wy
     * @param $WLng - point Wx
     *
     * @return float
     */
    public static function countAngleWigigFs($ALat, $ALng, $BLat, $BLng, $WLat, $WLng)
    {
        /*
         * u = AW = W-A
         * v = AB = B-A
         *
         * A-B = FS
         * W = wigig
         *
         * oA
         * |\
      v_w  | _fs
         * |  \
         * oW  \
         *      oB
         */
        $vectorW = ["x" => $WLng - $ALng, 'y' => $WLat - $ALat];
        $vectorFS = ["x" => $BLng - $ALng, 'y' => $BLat - $ALat];

        return self::countAngle($vectorW, $vectorFS);
    }

    /**
     * counts angle between two vectors where WIGIG is in the middle
     *
     * @param $ALat - point Ay
     * @param $ALng - point Ax
     * @param $WLat - point Wy
     * @param $WLng - point Wx
     * @param $direction
     *
     * @return float
     */
    public static function countOmega($ALat, $ALng, $WLat, $WLng, $direction)
    {
        /*
 [$ALat, $ALng]
   +          .
    \   |    .
     \  |   .
      \B|A .
       \|.
        + [$WLat, $WLng]
        |
        |
       axe

        output: angles A + B
        dotted direction is direction of Wigig (angle A)

         */
        $pointWigig = self::countPoint(['x' => $WLng, 'y' => $WLat], $direction);

        $vectorW = ["x" => $pointWigig['x'] - $WLng, 'y' => $pointWigig['y'] - $WLat];
        $vectorFS = ["x" => $ALng - $WLng, 'y' => $ALat - $WLat];

        return self::countAngle($vectorW, $vectorFS);
    }

    public static function countAngle($vectorA, $vectorB)
    {
        $top = $vectorA['x'] * $vectorB['x'] + $vectorA['y'] * $vectorB['y'];
        $bottom = sqrt($vectorA['x'] * $vectorA['x'] + $vectorA['y'] * $vectorA['y'])
            * sqrt($vectorB['x'] * $vectorB['x'] + $vectorB['y'] * $vectorB['y']);

        $cosPhi = ($bottom > 0) ? $top / $bottom : $top;
        if (strval($cosPhi) == '1') {
            return 0;
        }
        if (strval($cosPhi) == '-1') {
            return 180;
        }
        $phi = rad2deg(acos($cosPhi));

        return $phi;
    }

    /**
     * counts a point moved by 1 in the direction defined by $alpha
     *
     * @param $point
     * @param $alpha - angle of direction
     *
     * @return array
     */
    public static function countPoint($point, $alpha): array
    {
        $x = 1;
        $y = 0;
        if ($alpha < 90) {
            // I
            $beta = 90 - $alpha;
            $y = self::countTan($beta);
        } elseif ($alpha > 90 && $alpha < 180) {
            // II
            $beta = $alpha - 90;
            $y = self::countTan($beta) * (-1);
        } elseif ($alpha > 180 && $alpha < 270) {
            // III
            $beta = 270 - $alpha;
            $x = -1;
            $y = self::countTan($beta) * (-1);
        } elseif ($alpha > 270) {
            // IV
            $beta = $alpha - 270;
            $x = -1;
            $y = self::countTan($beta);
        } elseif ($alpha == 90) {
            $x = 1;
            $y = 0;
        } elseif ($alpha == 180) {
            $x = 0;
            $y = -1;
        } elseif ($alpha == 270) {
            $x = -1;
            $y = 0;
        } elseif ($alpha == 360 || $alpha == 0) {
            $x = 0;
            $y = 1;
        }
        return ['x' => $point['x'] + $x, 'y' => $point['y'] + $y];
    }

    /**
     * counts tan for $beta angle . In DEG out DEG
     *
     * @param $beta
     *
     * @return float
     */
    public static function countTan($beta): float
    {
        $out = tan(deg2rad($beta));
        return $out;
    }

    /**
     * returns distance in KM
     *
     * @param $latA
     * @param $lngA
     * @param $latB
     * @param $lngB
     * @param float $earthRadius
     *
     * @return float|int
     */
    public static function countDistance($latA, $lngA, $latB, $lngB, $earthRadius = 6378.137)
    {
        // convert from degrees to radians
        $latFrom = deg2rad($latA);
        $lonFrom = deg2rad($lngA);
        $latTo = deg2rad($latB);
        $lonTo = deg2rad($lngB);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * $earthRadius;
    }


    public static function findAndSetClosestLattgas(Station &$station)
    {
        if ($station->isFs() && !$station->frequency) {
            throw new Exception('no frequency');
        }

        $qTemplate = "
        (SELECT lattgas as ltgs
        FROM (
            (SELECT `lattgas`, ABS(`frequency` - :val:) AS distance
              FROM `gases`
              WHERE `frequency` >= :val:
              order BY frequency
              )
             UNION
             (SELECT `lattgas`, ABS(`frequency` - :val:) AS distance
              FROM `gases`
              WHERE `frequency` <= :val:
              order BY frequency DESC
              )
             order by distance
             ) tmp)";

        $q = str_replace(':val:', ($station->frequency / 1000), $qTemplate);
        $data = \Yii::$app->db->createCommand($q)->queryAll();

        $station->specific_run_down = $data[0]['ltgs'];
    }

    /**
     * @param $phi
     * @param $antennaVolume
     *
     * @return mixed
     * @throws Exception
     */
    public static function countAdPhi($phi, $antennaVolume)
    {
        $qTpl = 'SELECT ad_phi, distance
 FROM (
          (SELECT `ad_phi`, ABS(`phi12` - :phiVal) AS distance
           FROM `phi`
           WHERE `phi12` >= :phiVal
             AND :gVal >= g_min
             AND :gVal < g_max
           ORDER BY `phi12`
           )
          UNION
          (SELECT `ad_phi`, ABS(`phi12` - :phiVal) AS distance
           FROM `phi`
           WHERE `phi12` <= :phiVal
             AND :gVal >= g_min
             AND :gVal < g_max
           ORDER BY `phi12`
           )
          UNION
          (SELECT `ad_phi`, ABS(`phi34` - :phiVal) AS distance
           FROM `phi`
           WHERE `phi34` >= :phiVal
             AND :gVal >= g_min
             AND :gVal < g_max
           ORDER BY `phi34`
           )
          UNION
          (SELECT `ad_phi`, ABS(`phi34` - :phiVal) AS distance
           FROM `phi`
           WHERE `phi34` <= :phiVal
             AND :gVal >= g_min
             AND :gVal < g_max
           ORDER BY `phi34`
           ) 
     ) tmp
 ORDER BY distance
 LIMIT 1
';

        $data = Yii::$app->db->createCommand($qTpl, [':gVal' => $antennaVolume, ':phiVal' => $phi])->queryAll();

        return $data[0]['ad_phi'];
    }

    public static function countAdPhiWigig($phi, $isPtmp)
    {
        $qTpl = 'SELECT ad, distance
 FROM (
          (
              SELECT `ad`, ABS(`phi` - :val) AS distance
              FROM `phi_wigig`
              WHERE `phi` >= :val AND `ptmp` = :is_ptmp
           )
          UNION
          (
              SELECT `ad`, ABS(`phi` - :val) AS distance
              FROM `phi_wigig`
              WHERE `phi` <= :val AND `ptmp` = :is_ptmp
           )
      ) tmp
 ORDER BY distance
 ';

        $data = Yii::$app->db->createCommand($qTpl, [':is_ptmp' => $isPtmp, ':val' => $phi])->queryAll();
        $out = $data[0]['ad'];
        return $out;
    }

}
