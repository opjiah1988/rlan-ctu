<?php
/*
* Copyright 2022 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>

<?php

namespace common\models;

use Yii;

/**
 * @property int $id
 * @property float $phi12
 * @property float $phi34
 * @property float $g_phi
 * @property float $ad_phi
 * @property float $g_min
 * @property float $g_max
 * @property string $note
 * @property int $deleted
 * @property int $created_at
 * @property int $updated_at
 */
class Phi extends \dactylcore