<?php
/*
* Copyright 2022 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>

<?php

namespace common\models;

use yiiehaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "probe".
 *
 * @property integer $id
 * @property string $name
 * @property string $status
 * @property string $description
 * @property string $access_token
 * @property integer $deleted
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ProbeScan[] $probeScans
 */
class Probe extends \dactylcore