<?php
/*
* Copyright 2022 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>

<?php

$config = [
    'modules' => [
        'dc-jobs' => [
            'class' => \dactylcore\jobs\Module::class,
        ],
        'dc-log' => [
            'class' => dactylcore\log\Module::class,
        ],
        'dc-user' => [
            'class' => dactylcore\user\Module::class,
        ],
    ],
    'components' => [
        'cdnUrlManager' => require(__DIR__.'/../../vendor/dactylcore/core/src/configs/partial/cdnUrlManager.php'),
    ],
    'bootstrap' => [
        'dc-jobs',
        'dc-log',
        'dc-user'
    ],
];

$out = \yii\helpers\ArrayHelper::merge($config,
    require(__DIR__ . '/../../vendor/dactylcore/core/src/configs/common/test.php'));
return \yii\helpers\ArrayHelper::merge($out, require __DIR__ . '/test-local.php');
