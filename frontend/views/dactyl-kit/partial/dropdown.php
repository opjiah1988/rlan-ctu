<?php
/*
* Copyright 2022 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>

<?php
$fakeModel = new \yiiase\DynamicModel((['typeEmpty', 'typeFilled', 'typeEmpty2', 'typeFilled2', 'typeFilled3', 'typeEmptyError', 'typeFilledError']));
$fakeModel->typeFilled = 0;
$fakeModel->typeFilled2 = 0;
$fakeModel->typeFilledError = 0;
$fakeModel->addRule(['typeFilledError'], 'compare', ['compareValue' => 10]);
$fakeModel->addRule(['typeEmptyError'], 'required');
$fakeModel->validate();
?>
<?php $form = rontend\widgets\ActiveForm::begin() ?>

    <strong>Empty</strong>
<?= $form->field($fakeModel, 'typeEmpty')->widget(\dactylcore