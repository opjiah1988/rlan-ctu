/*
* Copyright 2022 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

function isInValid() {
    return $("form#station-form .form-group").hasClass('has-error');
}


function isEmpty() {
    return $("#station-a-lng").val() === ''
        || $("#station-a-lat").val() === ''
        || $("#station-b-lat").val() === ''
        || $("#station-b-lng").val() === '';
}


function initFs() {
    let app = new Vue({
        map: undefined,
        el: '#vueappfs',
        data: {
            locationLatA: document.querySelector("input[name='Station[a][lat]']").value,
            locationLngA: document.querySelector("input[name='Station[a][lng]']").value,
            locationLatB: document.querySelector("input[name='Station[b][lat]']").value,
            locationLngB: document.querySelector("input[name='Station[b][lng]']").value,
            distance: "",
            distanceWarning: true,
            distanceHide: true,
            myOnly: false,
        },

        methods: {
            showAll(){
                this.map.setMyOrAll(false);
            },
            showMy(){
                this.map.setMyOrAll(true);
            },

            changeA: function (event) {
                if (this.locationLngA && this.locationLatA) {
                    this.map.moveMarkerA(this.locationLngA, this.locationLatA);
                }
            },
            changeB: function (event) {
                if (this.locationLngB && this.locationLatB) {
                    this.map.moveMarkerB(this.locationLngB, this.locationLatB);
                }
            },
        },

        created: function () {
            this.map = new MapFs();
            if (this.locationLatA && this.locationLngA && this.locationLatB && this.locationLngB) {
                this.map.destinationToFly = [
                    (parseFloat(this.locationLngA) + parseFloat(this.locationLngB)) / 2,
                    (parseFloat(this.locationLatA) + parseFloat(this.locationLatB)) / 2
                ];
            }
            this.map.runMap('map-fs');
            this.map.vm = this;
        }
    });

    $(document).ready(function () {
        if (isEmpty() || app.$data.distanceWarning) {
            $(".loading-button").fadeOut('slow');
        } else {
            $(".loading-button").fadeIn('slow');
        }

        $('form#station-form').on('ajaxComplete', function () {
            if (isInValid() || isEmpty() || app.$data.distanceWarning) {
                $(".loading-button").fadeOut('slow');
            } else {
                $(".loading-button").fadeIn('slow');
            }
        });
    });
}

initFs();
