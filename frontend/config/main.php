<?php
/*
* Copyright 2022 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>

<?php
//Uses
use dactylcore\systemdmin\helpers\RssHelper;
use dactylcore\systemdmin\helpers\SitemapHelper;

$frontendConfig = [
    'params' => require __DIR__ . '/params.php',
    'bootstrap' => [
        'dc-menu',
        'dc-page',
        'dc-user',
//        'headers',
        function ()
        {

            Yii::$app->urlManager->addRules([
                _t('select-user-type', 'url_rules') => '/dc-user/user/select-user-type',
                _t('register-company', 'url_rules') => '/dc-user/user/register-company',
                _t('login_url', 'url_rules') => '/dc-user/user/login',
                _t('logout_url', 'url_rules') => '/dc-user/user/logout',
                _t('forgotten_password_url', 'url_rules') => '/dc-user/user/forgotten-password',
//                _t('profile_url', 'url_rules') => '/dc-user/user/index',

                // Admin user actions
//                _t('users', 'url_rules') => '/dc-user/user/index',
                _t('users/create', 'url_rules') => '/dc-user/user/create',
                _t('users/update', 'url_rules') => '/dc-user/user/update',
                _t('users/delete', 'url_rules') => '/dc-user/user/delete',

                // Admin user role actions
                _t('user-roles', 'url_rules') => '/dc-user/user-role/index',
                _t('user-roles/create', 'url_rules') => '/dc-user/user-role/create',
                _t('user-roles/update', 'url_rules') => '/dc-user/user-role/update',
                _t('user-roles/delete', 'url_rules') => '/dc-user/user-role/delete',
            ]);

            Yii::$app->urlManager->addRules([
                Yii::createObject([
                    'class' => dactylcore