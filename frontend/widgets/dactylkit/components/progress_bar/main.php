<?php
/*
* Copyright 2022 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>

<?php

/**
 * @var $step int
 * @var $steps array
 */

$out = '';
$stepSize = 100 / count($steps);

foreach ($steps as $index => $currentStep) {
    $class = 'dk--stepper-step';

    if ($index < $step) {
        $class .= ' dk--stepper-step--complete';
    }
    if ($index == $step) {
        $class .= ' dk--stepper-step--current';
    }

    $out .= "<li class='{$class}'>$currentStep</li>";
}

//progress-bar width ->
//      progress bar has to be shorter that it does not overlap next step label, or does not reach end in the final step
$progressBarWidth = $step * $stepSize - 5;
?>
    <div>
    <div class="progress">
        <div class="progress-bar" role="progressbar" aria-valuenow="0" style="width: <?= $progressBarWidth > 100 ? 100 : $progressBarWidth ?>%"
             aria-valuemin="0"
             aria-valuemax="<?= $step * $stepSize ?>"></div>
    </div>
    <ul class="dk--stepper">
        <?= $out ?>
    </ul>
    </div><?php
