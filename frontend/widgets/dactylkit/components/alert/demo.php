<?php
/*
* Copyright 2022 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>

<?php

use frontend\widgets\dactylkit\DactylKit;

?>


<div class="container dactyl-kit-demo">
    <h1>DactylKit components</h1>

    <div>
        <h2>Alert</h2>

        <style>
            .dactyl-kit-demo .dk-alert {
                margin: 32px 0;
            }
        </style>

        <div>
            <?= DactylKit::alert(
                'Default alert',
                'Pretium viverra suspendisse potenti nullam ac. Cras sed felis eget velit aliquet. Suspendisse potenti nullam ac tortor.',
                DactylKit::ALERT_TYPE_DEFAULT,
            ) ?>
            <?= DactylKit::alert(
                'Informational alert',
                'Pretium viverra suspendisse potenti nullam ac. Cras sed felis eget velit aliquet. Suspendisse potenti nullam ac tortor.',
                DactylKit::ALERT_TYPE_INFO
            ) ?>
            <?= DactylKit::alert(
                'Success alert',
                'Pretium viverra suspendisse potenti nullam ac. Cras sed felis eget velit aliquet. Suspendisse potenti nullam ac tortor.',
                DactylKit::ALERT_TYPE_SUCCESS
            ) ?>
            <?= DactylKit::alert(
                'Warning alert',
                'Pretium viverra suspendisse potenti nullam ac. Cras sed felis eget velit aliquet. Suspendisse potenti nullam ac tortor.',
                DactylKit::ALERT_TYPE_WARNING
            ) ?>
            <?= DactylKit::alert(
                'Critical alert',
                'Pretium viverra suspendisse potenti nullam ac. Cras sed felis eget velit aliquet. Suspendisse potenti nullam ac tortor.',
                DactylKit::ALERT_TYPE_CRITICAL
            ) ?>
        </div>

        <div>
            <h2>Variation</h2>
            <?= DactylKit::alert(
                'With button',
                'text.',
                DactylKit::ALERT_TYPE_DEFAULT,
                false,
                '',
                false,
                [],
                'Label',
                url(['/']),
            ) ?>

            <?= DactylKit::alert(
                'Close button',
                'Text',
                DactylKit::ALERT_TYPE_DEFAULT,
                false,
                '',
                true,
            ) ?>

            <?= DactylKit::alert(
                '',
                'Without title.',
                DactylKit::ALERT_TYPE_INFO
            ) ?>

            <?= DactylKit::alert(
                'Without text',
                '',
                DactylKit::ALERT_TYPE_INFO
            ) ?>

            <?= DactylKit::alert(
                'Title only',
                '',
                DactylKit::ALERT_TYPE_INFO,
                true,
                '',
            ) ?>

            <?= DactylKit::alert(
                '',
                'Text only',
                DactylKit::ALERT_TYPE_INFO,
                true,
                '',
            ) ?>

            <?= DactylKit::alert(
                'Thin top border',
                'Text',
                DactylKit::ALERT_TYPE_INFO,
                false,
            ) ?>

            <?= DactylKit::alert(
                '',
                'Custom icon',
                DactylKit::ALERT_TYPE_INFO,
                false,
                '@frontend/web/source_assets/img/ic-slash-20.svg',
            ) ?>
        </div>
    </div>
</div>
