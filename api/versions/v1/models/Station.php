<?php
/*
* Copyright 2022 DactylGroup s.r.o.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
will be approved by the European Commission - subsequent
versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl5
*
* Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
* See the Licence for the specific language governing
permissions and limitations under the Licence.
*/
?>

<?php

namespace apiersions1\models;


/**
 * @OA\Schema(
 *     schema="stationGeoJSON",
 *     description="GeoJSON for model Station",
 *     type="object",
 *     @OA\Property(
 *        title="type",
 *        property="type",
 *        type="string"
 *     ),
 *     @OA\Property(
 *        title="features",
 *        property="features",
 *        type="array",
 *        @OA\Items(
 *           @OA\Property(
 *              title="type",
 *              property="type",
 *              type="string"
 *           ),
 *           @OA\Property(
 *              title="geometry",
 *              property="geometry",
 *              type="object",
 *              @OA\Property(
 *                 title="type",
 *                 property="type",
 *                 type="string"
 *              ),
 *              @OA\Property(
 *                 title="coordinates in form [lng, lat]",
 *                 property="coordinates",
 *                 type="array",
 *                 @OA\Items()
 *              ),
 *           ),
 *           @OA\Property(
 *              title="properties",
 *              property="properties",
 *              type="object",
 *              @OA\Property(
 *                 title="t",
 *                 property="coordinates",
 *                 type="string"
 *              ),
 *              @OA\Property(
 *                 title="Wigig LAT point",
 *                 property="w_lat",
 *                 type="number"
 *              ),
 *              @OA\Property(
 *                 title="Wigig LNG point",
 *                 property="w_lng",
 *                 type="number"
 *              ),
 *           ),
 *           @OA\Property(
 *              title="id",
 *              property="id",
 *              type="string",
 *           ),
 *       )
 *   )
 *)
 */

/**
 * @OA\Schema(
 *     schema="stationItemLong",
 *     description="data for model Station",
 *     type="object",
 *     @OA\Property(
 *       title="id",
 *       property="id",
 *       type="integer"
 *     ),
 *     @OA\Property(
 *       title="id_station_pair",
 *       property="id_station_pair",
 *       type="integer"
 *     ),
 *     @OA\Property(
 *       title="position in FS pair A or B",
 *       property="pair_position",
 *       type="string"
 *     ),
 *     @OA\Property(
 *       title="type - wigig or fs",
 *       property="type",
 *       type="string"
 *     ),
 *     @OA\Property(
 *       title="type",
 *       property="typeName",
 *       type="string"
 *     ),
 *     @OA\Property(
 *       title="formattedId",
 *       property="formattedId",
 *       type="string"
 *     ),
 *     @OA\Property(
 *       title="name",
 *       property="name",
 *       type="string"
 *     ),
 *     @OA\Property(
 *       title="lat",
 *       property="lat",
 *       type="number"
 *     ),
 *     @OA\Property(
 *       title="lng",
 *       property="lng",
 *       type="number"
 *     ),
 *     @OA\Property(
 *       title="id_user",
 *       property="id_user",
 *       type="integer"
 *     ),
 *     @OA\Property(
 *       title="angle",
 *       property="angle",
 *       type="number"
 *     ),
 * )
 */

/**
 * @OA\Schema(
 *     schema="stationItemShort",
 *     description="data for model Station",
 *     type="object",
 *     @OA\Property(
 *       title="id",
 *       property="id",
 *       type="integer"
 *     ),
 *     @OA\Property(
 *       title="id_station_pair",
 *       property="ip",
 *       type="integer"
 *     ),
 *     @OA\Property(
 *       title="id master",
 *       property="m",
 *       type="integer"
 *     ),
 *     @OA\Property(
 *       title="id master",
 *       property="id_m",
 *       type="integer"
 *     ),
 *     @OA\Property(
 *       title="position in FS pair A or B",
 *       property="pp",
 *       type="string"
 *     ),
 *     @OA\Property(
 *       title="type - wigig or fs",
 *       property="t",
 *       type="string"
 *     ),
 *     @OA\Property(
 *       title="type",
 *       property="tn",
 *       type="string"
 *     ),
 *     @OA\Property(
 *       title="name",
 *       property="t",
 *       type="string"
 *     ),
 *     @OA\Property(
 *       title="lat",
 *       property="lt",
 *       type="number"
 *     ),
 *     @OA\Property(
 *       title="lng",
 *       property="lg",
 *       type="number"
 *     ),
 *     @OA\Property(
 *       title="id_user",
 *       property="u",
 *       type="integer"
 *     ),
 *     @OA\Property(
 *       title="angle of wigig",
 *       property="a",
 *       type="number"
 *     ),
 * )
 */

/**
 * @OA\Schema(
 *     schema="stationExclusionZone",
 *     description="data for model Station",
 *     type="object",
 *     @OA\Property(
 *        title="type",
 *        property="type",
 *        type="string"
 *     ),
 *     @OA\Property(
 *        title="features",
 *        property="features",
 *        type="array",
 *        @OA\Items(
 *           @OA\Property(
 *              title="type",
 *              property="type",
 *              type="string"
 *           ),
 *           @OA\Property(
 *              title="geometry",
 *              property="geometry",
 *              type="object",
 *              @OA\Property(
 *                 title="type",
 *                 property="type",
 *                 type="string"
 *              ),
 *              @OA\Property(
 *                 title="coordinates in form [lng, lat]",
 *                 property="coordinates",
 *                 type="array",
 *                 @OA\Items()
 *              ),
 *           ),
 *           @OA\Property(
 *              title="properties",
 *              property="properties",
 *              type="object",
 *              @OA\Property(
 *                 title="type",
 *                 property="t",
 *                 type="string"
 *              ),
 *              @OA\Property(
 *                 title="conflict",
 *                 property="c",
 *                 type="integer"
 *              ),
 *           ),
 *           @OA\Property(
 *              title="id",
 *              property="id",
 *              type="integer",
 *           ),
 *       )
 *   )
 *)
 */
class Station extends 